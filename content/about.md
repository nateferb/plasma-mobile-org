---
title: About
menu:
  main:
    parent: project
    weight: 2
--- 

Plasma Mobile is an open-source user interface and ecosystem targeted at mobile devices, built on the **KDE Plasma** stack.

A pragmatic approach is taken that is inclusive to software regardless of toolkit, giving users the power to choose whichever software they want to use on their device.

Plasma Mobile aims to contribute to and implement open standards, and is developed in a transparent process that is open for anyone to participate in.

<img src="/img/konqi/konqi-calling.png" width=250px/>

---

## Can I use it?

Yes! Plasma Mobile is currently preinstalled on the PinePhone using Manjaro ARM. There are also various other distributions for the PinePhone with Plasma Mobile available.

There is also postmarketOS, an Alpine Linux-based distribution with support for many more devices and it offers Plasma Mobile as an available interface for the devices it supports. You can see the list of supported devices [here](https://wiki.postmarketos.org/wiki/Devices), but on any device outside the main and community categories your mileage may vary.

The interface is using KWin over Wayland and is now mostly stable, albeit a little rough around the edges in some areas. A subset of the normal KDE Plasma features are available, including widgets and activities, both of which are integrated into the Plasma Mobile UI. This makes it possible to use and develop for Plasma Mobile on your desktop/laptop.

## What can it do?

There are quite a few touch-optimized apps that are now being bundled with the Manjaro-based Plasma Mobile image, allowing a wide range of basic functions. 

These are mostly built with Kirigami, KDE’s interface framework allowing convergent UIs that work very well in a touch-only environment. 

You can find a list of these applications on [plasma-mobile.org homepage](https://plasma-mobile.org).

## Where can I find…

The code for various Plasma Mobile components can be found on [invent.kde.org](https://invent.kde.org/plasma-mobile).

Read the [contributors documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home) for more details on the Plasma stack and how everything fits together.

You can also ask your questions in the [Plasma Mobile community groups and channels](/join).
