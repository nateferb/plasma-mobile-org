---
title: "Winners of the KDE and LMMS Plasma Mobile Sound Contest"
SPDX-License-Identifier: CC-BY-4.0
date: 2021-07-08
author: Plasma Mobile and LMMS team
authors:
- SPDX-FileCopyrightText: 2021 Paul Brown <paul.brown@kde.org>
- SPDX-FileCopyrightText: 2021 Guilherme Marçal Silva <guimarcalsilva@gmail.com>
---
<img width="800" src="winners_main_post.png">

Plasma Mobile just got a brand new selection of ringtones, alarms and system sounds thanks to the participants of the KDE/LMMS Plasma Mobile Sound Contest. Judges from both communities selected a variety of sounds made specifically for Plasma Mobile and now we are thrilled to announce the winners!

### Runner Ups

These are the very talented musicians and sound engineers who generously provided the Plasma Mobile project with cool alarms, ringtones and a variety of other interesting sounds:

* **Shell Raiser:** Check out their [latest song on SoundCloud](https://soundcloud.com/user-185450671/energy). Follow Shell Raiser on [Instagram](https://instagram.com/raise_da_bass) and check out their [YouTube channel](https://www.youtube.com/channel/UCmEShK3aru1usVrGOrzGqqw).
* **Neyrax:** You can follow Neyrax on [Twitter](https://twitter.com/NeyraXOfficial) and check out their work on [Audius](https://audius.co/neyraxofficial).
* **Axel Lopez:** Axel is also on [Twitter](https://twitter.com/akashii_lpz) and publishes their work on [SoundCloud](https://soundcloud.com/akashii_lpz).
* **Ivan Kiselyov:** You can follow Ivan on his [Telegram channel](https://t.me/typeyourtexthereblog).
* **Technology Sound Massacre (TSM):** Check out TSM's work on [SoundCloud](https://soundcloud.com/tesoma).
* **Derek Lin:** Listen to Derek's work on [SoundCloud](https://soundcloud.com/kenoimusic).

### Grand Winner

**Nesdood007** won the competition, but it was a close race until the end. You can listen to [Nesdood007's work on SoundCloud](https://soundcloud.com/nesdood007), and he will be receiving a selection of KDE goodies as a prize.

You can download all the sounds that will be included in Plasma Mobile from the [KDE Invent repository](https://invent.kde.org/devinlin/plasma-mobile-sounds/-/tree/master).

If you like what you hear and would like to contact any of the participants for  professional sound work, email us at press@kde.org and we will provide you with their contact details.

We would like to thank everyone involved in this contest, including all participants, judges, organizers and the LMMS community at large.
