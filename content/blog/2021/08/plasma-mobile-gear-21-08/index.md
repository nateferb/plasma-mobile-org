---
title: "Plasma Mobile Gear 21.08 is Out"
subtitle: "insert subtitle here"
SPDX-License-Identifier: CC-BY-4.0
date: 2021-08-31
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2021 Bhushan Shah <bshah@kde.org>
- SPDX-FileCopyrightText: 2021 Devin Lin <espidev@gmail.com>
- SPDX-FileCopyrightText: 2021 Bart De Vries <bart@mogwai.be>
kasts:
  - name: Kasts Storage Location Settings
    url: kasts-storage-location.png
---

The Plasma Mobile team is happy to present the Plasma Mobile updates for August 2021. Read on to find out about all the new stuff included in this release:

## Shell

Aleix Pol fixed many issues in KWin affecting the virtual keyboard, greatly improving the reliability of bringing it up and closing it when not needed.

He also rewrote the top panel quick settings code, putting in place the infrastructure that will facilitate making custom quick setting buttons.

## Clock

Boris Petrov added a feature that allows you to loop timers. This will let you set timers to loop indefinitely to do repetitive tasks for instance. Boris also added the option of being able to specify custom commands to be run when a timer finishes.

Devin fixed window-sizing issues with small heights, improving the experience in landscape mode on the Pinephone. He also enhanced some of the animations throughout the application.

Nicolas Fella did some refactoring in the application.

![New Timer Features](kclock.png)

## Weather

Nicolas Fella did some major refactoring of the application as well as the backend library it depends on.

Han Young and Devin Lin switched the weather chart to use Qt Quick charts, fixing issues with overly thick lines being rendered on the Pinephone.

## Kasts

Kasts, Plasma Mobile's podcast app, apart from getting some minor bugfixes, now lets you change the location where the downloaded podcast episodes and cached images are saved. These files can take up a lot of disk space, so it is nice to be able to store them elsewhere on systems with limited home folder disk space. The related settings now show the disk space used by episodes and images, and allows to clear the image cache.

{{< screenshots name="kasts" >}}

## Spacebar

Spacebar is Plasma Mobile's app for receiving and sending SMS.

Smitty van Bodegom fixed the message state code in Spacebar to correctly show in all cases when sending a message failed. Additionally, he fixed a common error that was caused by sending the phone number to oFono in a format that it didn't understand.
