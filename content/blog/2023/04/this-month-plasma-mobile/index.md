---
title: "This Month in Plasma Mobile: April 2023"
subtitle: "We continued the port to Qt6, released the 23.04 release and attended Linux App Summit"
SPDX-License-Identifier: CC-BY-4.0
date: 2023-05-05
author: Plasma Mobile Team
authors:
- SPDX-FileCopyrightText: 2023 Devin Lin <devin@kde.org>
- SPDX-FileCopyrightText: 2023 Carl Schwan <carl@carlschwan.eu>
cssFiles:
- css/swiper-bundle.min.css
scssFiles:
- scss/components/swiper.scss
minJsFiles:
- js/swiper-bundle.min.js
jsFiles:
- js/swiper-init.js
images:
 - scale-1.jpg
powerplant:
- name: Homepage
  url: powerplant-mobile-home.png
- name: Editor
  url: powerplant-mobile-editor.png
- name: Detail about a plant
  url: powerplant-mobile-detail.png
---

We have been busy this past month! We released KDE Gear 23.04, which now contains most of the Plasma Mobile applications. The development of Plasma Shell based on Qt6 is progressing.

## Plasma

Daniel Hofmann reduced the refresh rate of the clock to reduce the energy consumption.

Devin continued porting to Qt6 and fixing bugs. He also added a first start wizard for when the user logs for the first time.

## Push notifications

Volker presented his work on Push Notifications at the Linux App Summit in Brno
last weekend. You can watch his talk on youtube.

<iframe width="560" height="315"
src="https://www.youtube-nocookie.com/embed/3-_aUNZMvko?start=4990"
title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;
clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
allowfullscreen></iframe>

## Tokodon (Mastodon client)

The visibility, post-creation time and application are now displayed when a
post is selected  (Shubham Arora, KDE Gear 24.08.0,
[Link](https://invent.kde.org/network/tokodon/-/merge_requests/201))

![Post creation information](post-creation-time.png)

We now always use relative times in posts (Shubham Arora, KDE Gear 24.08.0,
[Link](https://invent.kde.org/network/tokodon/-/merge_requests/203))

We now consistently use "Post" in our UI instead of a mix between "Post" and "Toot".
(Shubham Arora, KDE Gear 24.08.0,
[Link](https://invent.kde.org/network/tokodon/-/merge_requests/209))

We implemented the Mastodon "Explore" tab, which shows some posts popular in your
instance (Shubham Arora, KDE Gear 24.08.0,
[Link](https://invent.kde.org/network/tokodon/-/merge_requests/205))

We overhauled the interaction icons (Joshua Goins, KDE Gear 23.08.0,
[Link](https://invent.kde.org/network/tokodon/-/merge_requests/211))

![New icons on desktop](tokodon-desktop.png)

![New icons on mobile evenenly spaced](tokodon-mobile.png)

## Kalendar

The contact editor now allows adding basic personal information about the
contact (Aakarsh MJ, KDE Gear 23.08.0,
[Link](https://invent.kde.org/pim/kalendar/-/merge_requests/345)) and also
their profile picture (Aakarsh MJ, KDE Gear 23.08.0,
[Link](https://invent.kde.org/pim/kalendar/-/merge_requests/332))

We fixed a serious crash when editing events in the calendar in the month view (Carl
Schwan, KDE Gear 23.04.1,
[Link](https://invent.kde.org/pim/kalendar/-/merge_requests/359))

We fixed a data race causing a crash (Carl Schwan, KDE Gear 23.04.1,
[Link](https://invent.kde.org/pim/kalendar/-/merge_requests/360))

We fixed the custom reminder option showing as "On Event start" (Vanshpreet S Kohli,
KDE Gear 23.04.1,
[Link](https://invent.kde.org/pim/kalendar/-/merge_requests/358))

We don't link against the libraries AkonadiXML, AkonadiAgentBase,
CalendarSupport (Volker Krause, KDE Gear 23.08.0,
[Link 1](https://invent.kde.org/pim/kalendar/-/merge_requests/354)
[Link 2](https://invent.kde.org/pim/kalendar/-/merge_requests/355))
and EventViews (Kevin Ottens - Carl Schwan, KDE Gear 23.08.0,
[Link 1](https://invent.kde.org/pim/kalendar/-/merge_requests/350),
[Link 2](https://invent.kde.org/pim/kalendar/-/merge_requests/349)) anymore.

We re-enabled the search collection to see all the open or closed calendar
invitations now that they work again in Akonadi (Carl Schwan, KDE Gear 23.08.0,
[Link](https://invent.kde.org/pim/kalendar/-/merge_requests/351))

We now have unit tests for the phone number model (Aakarsh MJ, KDE Gear 23.08.0,
[Link](https://invent.kde.org/pim/kalendar/-/merge_requests/356))

## NeoChat

Tobias has reworked the Quickswitcher (Ctrl+K) to be more useful. Now it shows the room's name and last message (NeoChat 23.08, [Link](https://invent.kde.org/network/neochat/-/merge_requests/878)).

Carl ported the spell-checking settings page to the newer form components (NeoChat 23.08, [Link](https://invent.kde.org/network/neochat/-/merge_requests/917)).

James has created improved full-window image and video-viewing components, which you will soon also see in more of our apps (NeoChat 23.08, [Link](https://invent.kde.org/network/neochat/-/merge_requests/824) and Kirigami Addons 0.8, [Link](https://invent.kde.org/libraries/kirigami-addons/-/merge_requests/88)).

Tobias has implemented support for sending location events (NeoChat 23.08, [Link](https://invent.kde.org/network/neochat/-/merge_requests/312))

Marc changed the message style to have a different background color for code blocks (NeoChat 23.08, [Link](https://invent.kde.org/network/neochat/-/merge_requests/867))

## Powerplant

Powerplant is a new application developed by Mathis with the help of Carl. This
application helps you keep track of your plants' needs (e.g., water).

![Powerplant home](powerplant.png)

![Powerplant detailed information about a plant with health history grath](powerplant-detail.png)

{{< screenshots name="powerplant" >}}

This is a new application and we welcome contributions. Check out our [Gitlab Repository](https://invent.kde.org/utilities/powerplant).

## MarkNote

Another new application by Mathis is MarkNote. MarkNote is a WYSIWYG note editor.

![Marknote](marknote.png)

This is a new application and we welcome contributions. Check out our [GitLab Repository](https://invent.kde.org/office/marknote).

## OptiImage

Yet another application is OptiImage, developed by Carl. It is an image
optimizer which makes use of optipng and jpegoptim to
optimize images.

![Image optimizer](image-optimizer.png)

This is a new application and we welcome contributions. Check out our [GitLab Repository](https://invent.kde.org/carlschwan/optiimage/).

## Audiotube

Audiotube now lets you right-click to open the context menu (Mathis, [Link](https://invent.kde.org/multimedia/audiotube/-/merge_requests/110)).

The top result is now displayed at the top of the search results (Jonah, [Link](https://invent.kde.org/multimedia/audiotube/-/merge_requests/111)). 

Many broken search terms were fixed (Jonah)

## Rattlesnake

The metronome app Rattlesnake was completely redesigned by Mathis and now uses the mobile form components and new sounds.

![Rattlesnake](rattlesnake.png)

Check out the [GitLab repository](https://invent.kde.org/multimedia/rattlesnake).

## Contributing

Want to help with the development of Plasma Mobile? We are ~~desperately~~ looking for new contributors, **beginners are always welcome**!

Take Plasma Mobile for a spin! Check out the [device support for each distribution](https://www.plasma-mobile.org/get/) and find the version which will work on your phone.

Even if you do not have a compatible phone or tablet, you can also help us out with application development, as you can easily do that from a desktop!

View our [documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/home), and consider joining our [Matrix channel](https://matrix.to/#/#plasmamobile:matrix.org), and let us know what you would like to work on!

Our [issue tracker documentation](https://invent.kde.org/plasma/plasma-mobile/-/wikis/Issue-Tracking) also gives information on how and where to report issues.

## Donate

And finally, KDE can’t work without financial support, so [consider making a donation](http://kde.org/fundraisers/yearend2022) today! This stuff ain’t cheap and KDE e.V. has ambitious hiring goals. We can’t meet them without your generous donations!
