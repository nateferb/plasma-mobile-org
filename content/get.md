---
title: Distributions offering Plasma Mobile
menu:
  main:
    weight: 4
    name: Install
scssFiles:
  - scss/get.scss
layout: get-involved
no_text: true
---

Listed below are distributions that ship Plasma Mobile.

Please check the information for each distribution to see if your device is supported.

## Mobile

### Manjaro ARM

![](/img/manjaro.svg)

Manjaro ARM is the Manjaro distribution, but for ARM devices. It's based on Arch Linux ARM, combined with Manjaro tools, themes and infrastructure to make install images for your ARM device.

[Website](https://manjaro.org) [Forum](https://forum.manjaro.org/c/arm/)

#### Download

* [Latest Stable (PinePhone)](https://github.com/manjaro-pinephone/plasma-mobile/releases)
* [Developer builds (Pinephone)](https://github.com/manjaro-pinephone/plasma-mobile-dev/releases)

#### Installation

For the PinePhone, you can find generic information on [Pine64 wiki](https://wiki.pine64.org/index.php/PinePhone_Installation_Instructions).

---

### postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS), is a touch-optimized, pre-configured Alpine Linux that can be installed on smartphones and other mobile devices. View the [device list](https://wiki.postmarketos.org/wiki/Devices) to see the progress for supporting your device.

For devices that do not have prebuilt images, you will need to flash it manually using the `pmbootstrap` utility. Follow instructions [here](https://wiki.postmarketos.org/wiki/Installation_guide). Be sure to also check the device's wiki page for more information on what is working.

[Learn more](https://postmarketos.org)

#### Download

* [Well supported devices](https://postmarketos.org/download/)
* [Full Device List](https://wiki.postmarketos.org/wiki/Devices)

#### Nightly Builds
Sineware [Plasma Mobile Nightly](https://sineware.ca/prolinux/plasma-mobile-nightly/) provides an unofficial Alpine repository and PinePhone/PinePhone Pro images, built nightly from git main/master. 

---

### Arch Linux ARM

![](/img/archlinux.png)

Arch Linux ARM has been ported to the PinePhone and PineTab by the DanctNIX community.

#### Download

* [Releases](https://github.com/dreemurrs-embedded/Pine64-Arch/releases)

---

### openSUSE

![](/img/openSUSE.svg)

openSUSE, formerly SUSE Linux and SuSE Linux Professional, is a Linux distribution sponsored by SUSE Linux GmbH and other companies. Currently openSUSE provides Tumbleweed based Plasma Mobile builds.

#### Download

* [PinePhone](https://download.opensuse.org/repositories/devel:/ARM:/Factory:/Contrib:/PinePhone/images/openSUSE-Tumbleweed-ARM-PLAMO-pinephone.aarch64.raw.xz)

---

### Fedora

![](/img/fedora.svg)

This is a work in progress, stay tuned!

Join the Fedora Mobility [matrix channel](https://matrix.to/#/#mobility:fedoraproject.org) to get details on the progress.

Links to testing images can be found in the channel.

---

### Debian

![](/img/debian.svg)

Debian does not package all of Plasma Mobile yet.

However, you can install [Mobian](https://mobian-project.org/) and follow the instructions to add a 3rd party repo providing Plasma Mobile packages [here](https://jbb.ghsq.de/debian-pm/).

---

## Desktop Devices

### Fedora

![](/img/fedora.svg)

Fedora has Plasma Mobile and related applications packaged in their repositories.

Install the [plasma-mobile](https://src.fedoraproject.org/rpms/plasma-mobile) package.

---

### postmarketOS

![](/img/pmOS.svg)

postmarketOS is able to be run in QEMU, and thus is a suitable option for trying Plasma Mobile on your computer.

Read more about it [here](https://wiki.postmarketos.org/wiki/QEMU_amd64_(qemu-amd64)). During the setup process, simply select Plasma Mobile as the desktop environment.

---

### Arch Linux

![](/img/archlinux.png)

Plasma Mobile is available on the [AUR](https://aur.archlinux.org/packages/plasma-mobile).

---

### Manjaro

![](/img/manjaro.svg)

Manjaro builds x64 images for use on desktop.

#### Download

* [Images](https://github.com/manjaro/manjaro-plasma-mobile-x64/releases)

---

### KDE Neon

![](/img/neon.svg)

**WARNING**: This is not actively maintained!

This image, based on KDE neon, can be tested on non-android intel tablets, PCs and virtual machines.

* [Neon amd64](https://files.kde.org/neon/images/mobile/)
